function* loop(){
  var array = [1,2,3,4,5,6,7,8,9];
  for(let item of array){
    console.log(item);
    yield ;
  }
}
var gen = loop();
console.log(gen.next().value);
