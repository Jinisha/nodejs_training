const func = require('./function')
data = [{
    'id': '1',
    'map_value': '1',
    'que_name': null,
    'question': 'who is your favourite singer??',
    'responses': [{
      'response_id': '1',
      'response1': 'Lata Mangeshkar',
      'map_valule': '100'
    },
    {
      'response_id': '2',
      'response1': 'Arijit singh',
      'map_valule': '0'
    },
    {
      'response_id': '3',
      'response1': 'Shreya Ghoshal',
      'map_valule': '102'
    },
   
    ]
  },
  {
    'id': '2',
    'map_value': undefined,
    'que_name': 'Hobbies',
    'question': 'what is your favourite passtime?',
    'responses': [{
      'response_id': '1',
      'response1': 'Mobile',
      'map_valule': '1',
      'abc': [{
        'name': '',
        'age': 22,
        'last': 'green'
      },
      {
        'name': 'color',
        'age': null,
        'last': 'blue'
      },
      {
        'name': 'color',
        'age': 22,
        'last': undefined
      }
      ]
    },
    {
      'response_id': '2',
      'response1': '',
      'map_valule': '106'
    },
    {
      'response_id': '3',
      'response1': 'cooking',
      'map_valule': '105'
    }
    ]
  }
  ]
  
  function display (data) {
    func.printLog(func.logCons.LOG_LEVEL_DEBUG,'display()', func.logCons.LOG_ENTER)
    var jsonVal = {}
    var arr = []
    for (var i in data) {
      var keyval = data[i]
      if (keyval === null || keyval === undefined || keyval === '') {
        jsonVal['type'] = keyval
        jsonVal['value'] = i
        arr.push(jsonVal)
        func.printLog(func.logCons.LOG_LEVEL_INFO,'Key having' + ' ' + keyval + ' ' + 'value is:' +i, func.logCons.LOG_ENTER) 
      } else if (typeof (keyval) === 'object') {
        display(keyval)
        func.printLog(func.logCons.LOG_LEVEL_INFO,'display()', func.logCons.LOG_EXIT)     
      }
  
    }
  
  }
  display(data)
  