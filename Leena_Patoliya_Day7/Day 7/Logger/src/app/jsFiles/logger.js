var json = [{
    'id': '1',
    'map_value': '1',
    'que_name': null,
    'q1': 'what is your favourite game?',
    'options': [{
        'option_id': '1',
        'option1': 'Cricket',
        'map_valule': '101'
      },
      {
        'option_id': '2',
        'option1': 'Hockey',
        'map_valule': '0'
      },
      {
        'option_id': '3',
        'option1': 'Kabaddi',
        'map_valule': '103'
      },
      {
        'option_id': '4',
        'option1': 'Kho-Kho',
        'map_valule': '110'
      }
    ]
  },
  {
    'id': '2',
    'map_value': undefined,
    'que_name': 'Hobbies',
    'q1': 'what is your favourite hobby?',
    'options': [{
        'option_id': '1',
        'option1': 'playing',
        'map_valule': '1',
        'jhgfd': [{
            'name': '',
            'age': 12,
            'last': 'patel'
          },
          {
            'name': 'Leena',
            'age': null,
            'last': 'patel'
          },
          {
            'name': 'Leena',
            'age': 12,
            'last': undefined
          }
        ]
      },
      {
        'option_id': '2',
        'option1': '',
        'map_valule': '107'
      },
      {
        'option_id': '3',
        'option1': 'cooking',
        'map_valule': '106'
      }
    ]
  }
]
const func = require('../utils/functions')

function display(json) {
  func.printLog(func.logCons.LOG_LEVEL_INFO, 'display()', func.logCons.LOG_ENTER)
  var keyValueJson = {}
  var keyValueArray = []
  for (var i in json) {
    try {
      var innerValue = json[i]
      if (innerValue === null || innerValue === undefined || innerValue === '') {
        keyValueJson['type'] = innerValue
        keyValueJson['value'] = i
        keyValueArray.push(keyValueJson)
        func.printLog(func.logCons.LOG_LEVEL_DEBUG, keyValueArray, func.logCons.LOG_EXIT)
        //console.log('Key having' + ' ' + innerValue + ' ' + 'value ', i)
      } else if (typeof(innerValue) === 'object') {
        func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'display()', func.logCons.LOG_EXIT)
        display(innerValue)
      }
    } catch (err) {
      func.printLog(func.logCons.LOG_LEVEL_ERROR, `Error. ${err}`)
    }
  }
}
display(json)
