function result() {
  var r1 = document.getElementById("i1").value
  var m1 = generateMatrixForm1(r1)
  var r2 = document.getElementById("i2").value
  var m2 = generateMatrixForm2(r2)
  valueMatcher(m1, m2)
}

function generateMatrixForm1(r1) {
  var cast;
  var arr = []
  var n
  newArr = new Array();
  var uniqueValueArray = []
  var matrixArray = [];
  var arrText = ''
  for (var i = 0; uniqueValueArray.length != 16; i++) {
    n = Math.floor(Math.random() * 17);
    matrixArray.push(n)
    uniqueValueArray = matrixArray.filter(function(elem, pos) {
      return matrixArray.indexOf(elem) == pos;
    });
  }
  
  var row1 = []
  var row2 = []
  var row3 = []
  var row4 = []
  var finalMatrix = []
  for (var j = 0; j < 4; j++) {
    row1.push(uniqueValueArray[j])
  }
  finalMatrix.push(row1)
  for (var k = 4; k < 8; k++) {
    row2.push(uniqueValueArray[k])
  }
  finalMatrix.push(row2)
  for (var l = 8; l < 12; l++) {
    row3.push(uniqueValueArray[l])
  }
  finalMatrix.push(row3)
  for (var m = 12; m < 16; m++) {
    row4.push(uniqueValueArray[m])
  }
  finalMatrix.push(row4)
  for (var z = 0; z < finalMatrix.length; z++) {
    document.getElementById("p" + z).innerHTML = finalMatrix[z]
  }
  if (r1 == 0) {
    document.getElementById("p4").innerHTML = "row number:" + row1
    return row1
  } else if (r1 == 1) {
    document.getElementById("p4").innerHTML = "row number:" + row2
    return row2
  } else if (r1 == 2) {
    document.getElementById("p4").innerHTML = "row number:" + row3
    return row3
  } else {
    document.getElementById("p4").innerHTML = "row number:" + row4
    return row4
  }
}


function generateMatrixForm2(r2) {
  var cast;
  var arr = []
  var n
  newArr = new Array();
  var uniqueValueArray = []
  var matrixArray = [];
  var arrText = ''
  for (var i = 0; uniqueValueArray.length != 16; i++) {
    n = Math.floor(Math.random() * 17);
    matrixArray.push(n)
    uniqueValueArray = matrixArray.filter(function(elem, pos) {
      return matrixArray.indexOf(elem) == pos;
    });
  }
  var rows1 = []
  var rows2 = []
  var rows3 = []
  var rows4 = []
  var finalMatrixs = []
  for (var b = 0; b < 4; b++) {
    rows1.push(uniqueValueArray[b])
  }
  finalMatrixs.push(rows1)
  for (var c = 4; c < 8; c++) {
    rows2.push(uniqueValueArray[c])
  }
  finalMatrixs.push(rows2)
  for (var d = 8; d < 12; d++) {
    rows3.push(uniqueValueArray[d])
  }
  finalMatrixs.push(rows3)
  for (var e = 12; e < 16; e++) {
    rows4.push(uniqueValueArray[e])
  }
  finalMatrixs.push(rows4)
  for (var o = 0; o < finalMatrixs.length; o++) {
    document.getElementById("z" + o).innerHTML = finalMatrixs[o]
  }
  if (r2 == 0) {
    document.getElementById("z4").innerHTML = "row number:" + rows1
    return rows1
  } else if (r2 == 1) {
    document.getElementById("z4").innerHTML = "row number:" + rows2
    return rows2
  } else if (r2 == 2) {
    document.getElementById("z4").innerHTML = "row number:" + rows3
    return rows3
  } else {
    document.getElementById("z4").innerHTML = "row number:" + rows4
    return rows4
  }
}

function valueMatcher(m1, m2) {
  var matchArray = []
  for (var i = 0; i < 4; i++) {
    if (m1[i] == m2[i]) {
      var arr = m1[i]
      matchArray.push(arr)
    }
    if (matchArray.length == 0) {
      document.getElementById("v1").innerHTML = "Volunteer Cheated"
    }
    if (matchArray.length == 1) {
      document.getElementById("v2").innerHTML = "Match Element:" + matchArray
    }
    if (matchArray.length > 1) {
      document.getElementById("v3").innerHTML = "Bad Magician"
    }
  }
}
