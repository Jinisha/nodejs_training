let jsonobj

  // load JSON file into javaobj
  function loadJson()
  {
    var xhttp = new XMLHttpRequest()
    xhttp.onreadystatechange = function(){
    if(xhttp.readyState==4 && xhttp.status==200)
      {
        jsonobj = JSON.parse(xhttp.response)
        formRegExp()
      }
    }
    xhttp.open("GET","conditions.json",true)
    xhttp.send()
  }

  // formation of Regular Expression
  function formRegExp(){
    let allow_alpha=jsonobj.allow_apha
    let allow_num = jsonobj.allow_num
    let allow_space = jsonobj.allow_space
    let length = jsonobj.length
    let sp_char=jsonobj.sp_char
    formation(allow_alpha,allow_num,allow_space,length,sp_char)
  }

  //built Regular Expression
 function formation(allow_alpha,allow_num,allow_space,length,sp_char)
  {
    str = '^['
    str+= for_alpha(allow_alpha)
    str+= for_num(allow_num)
    str+= for_space(allow_space)
    str+= for_spchar(sp_char)
    str+=']'
    str+= for_length(length)
    str+='$'
    console.log(str)
    return str
  }

  //check alphabetic conditions
  function for_alpha(allow_alpha){
    if(allow_alpha)
    {
      return 'a-zA-Z'
    }
    return ''
  }

  //check numeric condition
  function for_num(allow_num){
    if(allow_num){
      return '0-9'
    }
    return ''
  }

  //check for space
  function for_space(allow_space){
    if(allow_space){
      return '\s'
    }
    return ''
  }

  //check for length
  function for_length(length){
    if(length=='*')
    {
      return '*'
    }
    return `{${length[0]},${length[1]}}`
  }

  //check for special character
  function for_spchar(sp_char)
  {
    if(sp_char.length>0){
      let str =''
      for(let i =0 ;i<sp_char.length;i++){
        str+=`\\${sp_char[i]}`
      }
      return str
    }
    return ''
  }
  