var renderedResponse = [];
var json = {
    "questions": [
      {
        "ques_id": 1,
        "link_id": 1,
        "ques": "What are your hobbies?",
        "answers": [
          {
            "link_id": 11,
            "answer": "Reading"
          },
          {
            "link_id": 12,
            "answer": "Sports"
          },
          {
            "link_id": 13,
            "answer": "Travelling"
          }
        ]
      },
      {
        "ques_id": 2,
        "link_id": 11,
        "ques": "What types of book you like to read?",
        "answers": [
          {
            "link_id": 21,
            "answer": "Motivational"
          },
          {
            "link_id": 22,
            "answer": "Drama"
          }
        ]
      },
      {
        "ques_id": 3,
        "link_id": 12,
        "ques": "Which sport you like the most?",
        "answers": [
          {
            "link_id": 31,
            "answer": "Cricket"
          },
          {
            "link_id": 32,
            "answer": "Hockey"
          },
          {
            "link_id": 33,
            "answer": "Football"
          }
        ]
      },
      {
        "ques_id": 4,
        "link_id": 13,
        "ques": "Which place would you like to visit?",
        "answers": [
          {
            "link_id": 41,
            "answer": "Kashmir"
          },
          {
            "link_id": 42,
            "answer": "Manali"
          },
          {
            "link_id": 43,
            "answer": "Kerla"
          }
        ]
      },
      {
        "ques_id": 5,
        "link_id": 21,
        "ques": "What is your favourite motivational speaker?",
        "answers": [
          {
            "link_id": 51,
            "answer": "Jack Ma"
          },
          {
            "link_id": 52,
            "answer": "Sandip Maheshwari"
          }
        ]
      },
      {
        "ques_id": 6,
        "link_id": 31,
        "ques": "Who is your favourite cricketer?",
        "answers": [
          {
            "link_id": 61,
            "answer": "Virat Kohli"
          },
          {
            "link_id": 62,
            "answer": "M. S. Dhoni"
          },
          {
            "link_id": 63,
            "answer": "Hardik Pandya"
          }
        ]
      }
    ]
  }

function getQuestion(id){
    var radios;
    var questionTobeRendered = json.questions.filter(function(element){
        if(element.link_id === id){
            return element;
    }
    });

    if(questionTobeRendered.length>0){
        questionTobeRendered = questionTobeRendered[0];
        question_id = questionTobeRendered.ques_id;
        document.getElementById('div').innerHTML = questionTobeRendered.ques;
        var renderAnswer = '';        
        for(display of questionTobeRendered.answers){
            r_id = display.link_id;
            ans = display.answer;
            renderAnswer += "<span onclick='captureResponse("+`"${ans}"`+","+r_id+","+question_id+","+`"${questionTobeRendered.ques}"`+")'><input type=\"radio\" name=\"r_button\" id=\""+ r_id +"\" />"+ ans +"</span>";
            document.getElementById('radioDiv').innerHTML = renderAnswer;
            radios = document.getElementById(r_id).checked;
        }
    }
    else{
       document.getElementsByTagName('body')[0].innerHTML = "THANK YOU";
       var lineBreak = document.createElement('br');
       var input = document.createElement('INPUT');
       input.setAttribute('type', 'button');         
       input.setAttribute('value', 'Show Response');
       input.setAttribute('id', 'response');
       input.setAttribute('name', 'response');  
       input.setAttribute('onclick', 'showResponse()'); 
       document.body.appendChild(lineBreak);                             
       document.body.appendChild(lineBreak);                     
       document.body.appendChild(input);      
    }
}

function captureResponse(ans,link_id,ques_id,question){
    var response = {
        "que_id":ques_id,
        "question":question,
        "answer":ans
    }
    renderedResponse.push(response);
    // Array.prototype.push.apply(response);
    getQuestion(link_id);
}
function showResponse(){
  for(show of renderedResponse){    
    var div = document.createElement('div'); 
  div.setAttribute('id',show.question);
  document.body.appendChild(div);
    var span = document.createElement('div'); 
  span.setAttribute('id',show.answer);
  document.body.appendChild(span); 
  document.getElementById(show.question).innerHTML = "Your Question: "+ show.question;   
  document.getElementById(show.answer).innerHTML = "Your Answer: "+ show.answer;       
  }
}
