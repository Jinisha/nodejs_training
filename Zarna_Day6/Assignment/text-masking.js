var email = 'srkaycg123@gmail.com',
    phone = '123456789'

//Method 1: Using Slice
function maskEntry(item, type) {
    switch (type) {
        case 'email':
            var parts = item.split('@'), len = parts[0].length
            return email.replace(parts[0].slice(1, -1), '*'.repeat(len - 2))
        case 'phone':
            return item[0] + '*'.repeat(item.length - 4) + item.slice(-3)
        default:
            console.log('Undefinded Type')
    }
}

console.log('Text masking Using Slice: ',maskEntry(email, 'email')) // z**********1@gmail.com
console.log(maskEntry(phone, 'phone')) // 7******898

//Method 2: Using regex
console.log('Text masking using regex: ',phone.replace(/(\d{1})(.*)(\d{3})/, '$1******$3'))
console.log(email.replace(/(\w{1})(.*)(\w{1})@(.*)/, '$1*******$3@$4'))