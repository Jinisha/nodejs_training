var quejson = {
    "questions": [{
            "que_id": 1,
            "follow_id": 1,
            "question": "What is your favourite Hobby???",
            "answers": [{
                    "ans_id": 1,
                    "follow_id": 11,
                    "answer": "Sports"
                },
                {
                    "ans_id": 2,
                    "follow_id": 12,
                    "answer": "Reading"
                },
                {
                    "ans_id": 3,
                    "follow_id": 13,
                    "answer": "Cooking"
                }
            ]
        },
        {
            "que_id": 2,
            "follow_id": 12,
            "question": "Which types of book you like to read most???",
            "answers": [{
                    "ans_id": 1,
                    "follow_id": 19,
                    "answer": "Motivational Books"
                },
                {
                    "ans_id": 2,
                    "follow_id": 12,
                    "answer": "Drama Book"
                },
                {
                    "ans_id": 3,
                    "follow_id": 13,
                    "answer": "Novel"
                }
            ]
        },
        {
            "que_id": 3,
            "follow_id": 11,
            "question": "What is your favourite Sports?",
            "answers": [{
                    "ans_id": 1,
                    "follow_id": 31,
                    "answer": "Cricket"
                },
                {
                    "ans_id": 2,
                    "follow_id": 32,
                    "answer": "Volleyball"
                },
                {
                    "ans_id": 3,
                    "follow_id": 33,
                    "answer": "Football"
                }
            ]
        },
        {
            "que_id": 4,
            "follow_id": 13,
            "question": "What type of cooking you like the most?",
            "answers": [{
                    "ans_id": 1,
                    "follow_id": 51,
                    "answer": "Gujarati"
                },
                {
                    "ans_id": 2,
                    "follow_id": 52,
                    "answer": "Italian"
                },
                {
                    "ans_id": 3,
                    "follow_id": 53,
                    "answer": "Panjabi"
                }
            ]
        },
        {
            "que_id": 5,
            "follow_id": 31,
            "question": "Who is your favourite cricketer?",
            "answers": [{
                    "ans_id": 1,
                    "follow_id": 61,
                    "answer": "Virat"
                },
                {
                    "ans_id": 2,
                    "follow_id": 62,
                    "answer": "Dhoni"
                },
                {
                    "ans_id": 3,
                    "follow_id": 63,
                    "answer": "Yuvraj"
                }
            ]
        }
    ]
}
var j1 = JSON.stringify(quejson.questions);

var ids = [];
function startQuiz(firstid) {
    var getquestions = "";
    getquestions = quejson.questions.filter(function (matchid) {
        if (matchid.follow_id == firstid) {
            ids.push(matchid);
            return matchid;
        } else {
            return null;
        }
    });
    loadQuestions(getquestions)
}

function loadQuestions(getquestions) {
    var follow_id, answerid, answer, finalAnswer = "";
    if (getquestions.length != 0) {
        getquestions = getquestions[0];
        var question = getquestions.question
        document.getElementById('que1').innerHTML = question;
        for (x of getquestions.answers) {
            follow_id = x.follow_id;
            answerid = x.ans_id;
            answer = x.answer;
            finalAnswer += "<input type=\"radio\" name=\"r1\" onclick='loadSubQuestions(" + answerid + "," + follow_id + ")'>" + answer + "</input>";
            document.getElementById('ans1').innerHTML = finalAnswer;
        }
    } else {
        document.getElementById('msg').innerHTML = "Thank You !!! Quiz over !! <br/><br/>";
        for (x of ids) {
            document.getElementById('printquestion').innerHTML += x.question + "<br>";
            document.getElementById('printanswer').innerHTML += x.answers[0].answer + "<br>";  
        }
    }
}

function loadSubQuestions(answerid, follow_id) {
    startQuiz(follow_id);
}