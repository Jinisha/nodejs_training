let regex = [{
    "name": "alphabet",
    "condition": "true",
    "regex": "a-zA-Z"
  },
  {
    "name": "number",
    "condition": "true",
    "regex": "0-9"
  },
  {
    "name": "whiteSpace",
    "condition": "true",
    "regex": "\\s"
  },
  {
    "name": "sp",
    "condition": "true",
    "regex": "\\.\\@\\%]"
  },
  {
    "name": "length",
    "condition": "true",
    "regex": "{5,9}"
  },
]

function load() {
  var length = regex.length;
  var regexArray = [];
  var isLength = 0;
  for (let i = 0; i < regex.length; i++) {
    if (regex[i].condition == "true") {
      if (regex[i].name === 'length' && regex[i].condition === 'true') {
        isLength = 1;
      }
      var array = regex[i].regex;
      regexArray.push(array);
    }
  }
  var expression = join(regexArray, isLength);
  return expression;
}

function join(regexArray, isLength) {
  var element = regexArray.join("");
  var show = document.getElementById("show");
  var RegExpression;
  if (isLength == 1) {
    RegExpression = "[" + element + "$";
    var regularExpression = new RegExp(RegExpression)
    show.innerHTML = "Regular Expression  is" + " " + regularExpression;
  } else {
    RegExpression = "[" + element + "]*$";
    var regularExpression = new RegExp(RegExpression)
    show.innerHTML = "regular Expression is" + " " + regularExpression;
  }
  return regularExpression;
}

function checkValid() {
  var exp = load();
  var showValid = document.getElementById("showValid");
  var value = document.getElementById("value").value;
  if (exp.test(value)) {
    showValid.innerHTML = "Valid";
  } else {
    showValid.innerHTML = "Not Valid";
  }
}
