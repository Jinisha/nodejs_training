let json = [
    {
      "field":"alphabets",
      "value":true,
      "pattern":"a-zA-Z"
    },
    {
      "field":"numbers",
      "value":true,
      "pattern":"0-9"
    },
    {
        "field":"whiteSpace",
        "value":true,
        "pattern":"\\s"
    },
    {
        "field":"characters",
        "value":true,
        "pattern":"\\.\\$\\@"
    },
    {
        "field":"length",
        "value":true,
        "pattern":"{5,20}"
    }
]
let regExpression;
function generateRegExpression(){
    let regExpstring = '^['
    let flag = false
    let length
    json.filter(function(element){
        if(element.value === true && element.field!=='length'){
            regExpstring += element.pattern
        }
        if(element.field === 'length'){
            flag = true
            length = element.pattern
        }
    });    
    if(flag === true){
            regExpstring += ']'+length+'$'
    }else{
        regExpstring += ']*$' 
    }
    regExpression = new RegExp(regExpstring)
    document.getElementById('displayRegEx').innerHTML = 'Regular Expression: '+regExpression        
}  
function validate(){
    let stringToValidate = document.getElementById('regExpInput').value
    if(regExpression.test(stringToValidate))
        document.getElementById('output').innerHTML = 'String is matched with Regular Expression'
    else
    document.getElementById('output').innerHTML = 'String is not matched with Regular Expression'        
}