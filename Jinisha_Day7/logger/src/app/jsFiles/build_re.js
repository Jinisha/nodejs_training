const func = require('../utils/function')
let jsonobj={
  "length":[3,7],
  "allow_apha":true,
  "allow_num":false,
  "allow_space":false,
  "sp_char":["@","$"]
}



  // formation of Regular Expression
  function formRegExp()
  {
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'formRegExp()', func.logCons.LOG_ENTER)
    let allow_alpha=jsonobj.allow_apha
    let allow_num = jsonobj.allow_num
    let allow_space = jsonobj.allow_space
    let length = jsonobj.length
    let sp_char=jsonobj.sp_char
    formation(allow_alpha,allow_num,allow_space,length,sp_char)
  }

  //built Regular Expression
  function formation(allow_alpha,allow_num,allow_space,length,sp_char)
  {
    try{
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'formation()', func.logCons.LOG_ENTER)
    str = '^['
    str+= forAlpha(allow_alpha)
    str+= forNum(allow_num)
    str+= forSpace(allow_space)
    str+= forSpchar(sp_char)
    str+=']'
    str+= forLength(length)
    str+='$'
    func.printLog(func.logCons.LOG_LEVEL_INFO, 'result '+ str, func.logCons.LOG_EXIT)
    return str
    }
    catch(err){
      func.printLog(func.logCons.LOG_LEVEL_ERROR, 'error: ' + err, func.logCons.LOG_EXIT)
    }
  }

  //check alphabetic conditions
  function forAlpha(allow_alpha){
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_alpha()', func.logCons.LOG_ENTER)
    if(allow_alpha)
    {
      func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_alpha()', func.logCons.LOG_EXIT)
      return 'a-zA-Z'
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_alpha()', func.logCons.LOG_EXIT)
    return ''
  }

  //check numeric condition
  function forNum(allow_num){
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_num()', func.logCons.LOG_ENTER)
    if(allow_num){
      func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_num()', func.logCons.LOG_EXIT)
      return '0-9'
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_num()', func.logCons.LOG_EXIT)
    return ''
  }

  //check for space
  function forSpace(allow_space){
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_space()', func.logCons.LOG_ENTER)
    if(allow_space){
      func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_space()', func.logCons.LOG_EXIT)
      return '\s'
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_space()', func.logCons.LOG_EXIT)
    return ''
  }

  //check for length
  function forLength(length){
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_length()', func.logCons.LOG_ENTER)
    if(length=='*')
    {
      func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_length()', func.logCons.LOG_EXIT)
      return '*'
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_length()', func.logCons.LOG_EXIT)
    return `{${length[0]},${length[1]}}`
  }

  //check for special character
  function forSpchar(sp_char)
  {
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_spchar()', func.logCons.LOG_ENTER)
    if(sp_char.length>0){
      let str =''
      for(let i =0 ;i<sp_char.length;i++){
        str+=`\\${sp_char[i]}`
      }
      func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_spchar()', func.logCons.LOG_EXIT)
      return str
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'for_spchar()', func.logCons.LOG_EXIT)
    return ''
    
  }
formRegExp()