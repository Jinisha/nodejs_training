function square(a, b) {
    var c = a / b;
    var obj = new Promise(function (resolve, reject) {
        if (c != 0) {
            resolve(c); 
        } else {
            var err = new Error('error');
            reject(err); 
        }
    });
    obj.then(function (c) {
        console.log("No of Block is:"+c); 
    }, function (err) {
        console.log(err); 
    });
}
square(200, 100);