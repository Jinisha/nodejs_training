let data = {
    "questions": [{
            "que_id": 1,
            "intent": undefined,
            "que_text": null,
            "responses": [{
                    "ans_id": 1,
                    "followup_id": null,
                    "answer": "Reading"
                },
                {
                    "ans_id": 2,
                    "followup_id": 12,
                    "answer": ""
                },
                {
                    "ans_id": 3,
                    "followup_id": 13,
                    "answer": ''
                }
            ]
        },
        {
            "que_id": 2,
            "intent": "Hobby",
            "que_text": "What is your favourite pastime?",
            "responses": []
        },
        {
            "que_id": 11,
            "intent": "Reading",
            "que_text": {},
            "responses": [{
                },
                {
                    "ans_id": 2,
                    "followup_id": undefined,
                    "answer": "Drama"
                },
                {
                    "ans_id": 3,
                    "followup_id": 17,
                    "answer": "Mystery"
                }
            ]
        },
        {
            "que_id": 16,
            "intent": "Reading",
            "que_text": "Which drama book you like most?",
            "responses": [{
                    "ans_id": 1,
                    "followup_id": 0,
                    "answer": "Thirteen Reasons Why"
                },
                {
                    "ans_id": 2,
                    "followup_id": 0,
                    "answer": "Before I Fall"
                },
                {
                    "ans_id": 3,
                    "followup_id": 2,
                    "answer": "Other"
                }
            ]
        },
        {
            "que_id": 15,
            "intent": "Reading",
            "que_text": "Which science fiction book you like most?",
            "responses": [{
                    "ans_id": null,
                    "followup_id": 0,
                    "answer": "Before mars"
                },
                {
                    "ans_id": 2,
                    "followup_id": 0,
                    "answer": "Iron Gold"
                },
                {
                    "ans_id": 3,
                    "followup_id": 2,
                    "answer": "Other"
                }
            ]
        }
    ]
}
const func = require('./utils/function')
var response = []
function validateJson(obj) {
    func.printLog(func.logCons.LOG_LEVEL_INFO, 'validateJson()', func.logCons.LOG_ENTER)
    for (let key in obj) {
        let child = obj[key]
        if (child === null || child === undefined || child ==='' || isEmpty(child)) {
            var emptyData = {}
            emptyData['key'] = key
            emptyData['value'] = child
            response.push(emptyData)
            func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'data: '+JSON.stringify(response) , func.logCons.LOG_EXIT)
        }
        else if (typeof (child) == "object") {
            mainIndex = data.questions.indexOf(child);
            validateJson(child)
        }
    }
    func.printLog(func.logCons.LOG_LEVEL_INFO, 'validateJson()', func.logCons.LOG_EXIT)
}

function isEmpty(child) {
    if(child instanceof Array) {
        return (child.length === 0)
    } else if (child instanceof Object) {
        return (Object.keys(child).length === 0);
    } else {
        return false
    }
}

validateJson(data)