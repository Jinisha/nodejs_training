var matrix = [];
var shuffledMatrix = [];
var firstSelectedRow = [];
var secondSelectedRow = [];
function getRandomMatrix(){
    var randomNumberArray=[];
    var ranNumber;
    var cardArray = [];
    var cardLength = 0;
    while( cardLength != 16) {
        ranNumber = Math.floor(Math.random() * (1 + 16 - 1)) + 1;
        randomNumberArray.push(ranNumber);
        cardArray = randomNumberArray.filter(function(elem, pos) {
            return randomNumberArray.indexOf(elem) == pos;
    });
    cardLength = cardArray.length;
    }
    return cardArray;
}
function displayMatrix(cardArray){
    let row = 0;
    let col = 0; 
    matrix[row] =[];

for(var i = 0;i<cardArray.length;i++){
    
    if( ( i%4 == 0 && col>0) ){
        col = 0;
        row++;
        matrix[row] =[];
     }   
    
        matrix[row][col] = cardArray[i];
    col++;

}   
}
function createMatrix (){
    var cardArray = [];
    cardArray = getRandomMatrix();
    displayMatrix(cardArray);
    document.getElementById('matrix').innerHTML = "";
for(var i = 0;i<4;i++){    
    for(var j = 0;j<4;j++){
     document.getElementById('matrix').innerHTML += matrix[i][j] + "  " ;
    }

     document.getElementById('matrix').innerHTML +=" <br>";

}
var input = document.createElement('INPUT');
input.setAttribute('type', 'number');
input.setAttribute('value', '');
input.setAttribute('id', 'column');
input.setAttribute('placeholder', 'Enter row Number');    
var lineBreak = document.createElement('br');
document.body.appendChild(input);
document.body.appendChild(lineBreak);
var button = document.createElement('INPUT');
    button.setAttribute('value', 'Get other matrix');
    button.setAttribute('type', 'button');    
    button.setAttribute('name', 'Get other matrix');    
    button.setAttribute('id', 'button');
    button.onclick = function() { 
        var firstRow= document.getElementById('column').value;  
        for(var i=0;i<4;i++){
            for(var j=0;j<4;j++){
                if((firstRow-1) == i){
                    firstSelectedRow.push((matrix[i][j]));
                }
            }
        }      
        getShuffledMatrix(firstRow);
      };
    document.body.appendChild(button);
    document.body.appendChild(lineBreak);

}
function getShuffledMatrix(firstRow){
    var shuffleArray;
    shuffleArray = getRandomMatrix();
    var row = 0;
    var col = 0; 
    shuffledMatrix[row] =[];

    for(var i = 0;i<shuffleArray.length;i++){
    
        if( ( i%4 == 0 && col>0) ){
            col = 0;
            row++;
            shuffledMatrix[row] =[];
         }   
        
         shuffledMatrix[row][col] = shuffleArray[i];
        col++;
    
    }   
    document.getElementById('shuffledatrix').innerHTML = "";
for(i = 0;i<4;i++){    
    for(var j = 0;j<4;j++){
     document.getElementById('shuffledatrix').innerHTML += shuffledMatrix[i][j] + "  " ;
    }
    document.getElementById('shuffledatrix').innerHTML +=" <br>"; 
}
var input = document.createElement('INPUT');
input.setAttribute('type', 'number');
input.setAttribute('value', '');
input.setAttribute('id', 'row');
input.setAttribute('placeholder', 'Enter row Number');    
var lineBreak = document.createElement('br');
document.body.appendChild(input);
document.body.appendChild(lineBreak);
}
function getResult(){
    var secondRow = document.getElementById('row').value;
    for(var i=0;i<4;i++){
        for(var j=0;j<4;j++){
            if((secondRow-1) == i){
                secondSelectedRow.push((shuffledMatrix[i][j]));
            }
        }
    }
    getFinalOutput();
}
function getFinalOutput() {
    var finalArray=[];
    firstSelectedRow.forEach((element1)=>secondSelectedRow.forEach((element2)=>{
    if(element1 === element2){
    finalArray.push(element1);
}}
));
if(finalArray.length === 0)
    document.getElementById('result').innerHTML = "Volunteer cheated";
else if(finalArray.length >= 2)
document.getElementById('result').innerHTML = "Bad Magician!";
else  
document.getElementById('result').innerHTML = "Your Guessed Answer: "+finalArray;
}