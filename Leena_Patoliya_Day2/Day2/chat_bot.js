var json = [{
    "id": "1",
    "map_value": "1",
    "que_name": "sport",
    "q1": "what is your favourite game?",
    "options": [{
        "option_id": "1",
        "option1": "Cricket",
        "map_valule": "101"
      },
      {
        "option_id": "2",
        "option1": "Hockey",
        "map_valule": "0"
      },
      {
        "option_id": "3",
        "option1": "Kabaddi",
        "map_valule": "103"
      },
      {
        "option_id": "4",
        "option1": "Kho-Kho",
        "map_valule": "110"
      }
    ]
  },
  {
    "id": "2",
    "map_value": "101",
    "que_name": "hobbies",
    "q1": "what is your favourite hobby?",
    "options": [{
        "option_id": "1",
        "option1": "playing",
        "map_valule": "1"
      },
      {
        "option_id": "2",
        "option1": "reading",
        "map_valule": "107"
      },
      {
        "option_id": "3",
        "option1": "cooking",
        "map_valule": "106"
      }
    ]
  },
  {
    "id": "3",
    "map_value": "103",
    "que_name": "cooking",
    "q1": "what is your favourite dish?",
    "options": [{
        "option_id": "1",
        "option1": "pasta",
        "map_valule": "105"
      },
      {
        "option_id": "2",
        "option1": "burger",
        "map_valule": "111"
      },
      {
        "option_id": "3",
        "option1": "pizza",
        "map_valule": "115"
      }
    ]
  }
]

function startExam() {
  var x = document.getElementById("rdDiv");
  if (x.style.display === "none") {
    x.style.display = "block";

  } else {
    x.style.display = "none";
  }
}

function next() {
  var x = document.getElementById("rdDiv");
  var rates = document.getElementsByName('r1');
  var rate_value;
  x.style.display = "none";
  for (var z = 0; z < rates.length; z++) {
    if (rates[z].checked) {
      rate_value = rates[z].value;
    }
  }
  displayQuestion(rate_value)
}

function displayQuestion(value) {
  var str = '';
  var z = document.getElementById("radioDiv");
  var obj = JSON.stringify(json)
  for (var i = 0; i < json.length; i++) {
    if (value == json[i]["que_name"]) {
      document.getElementById("l1").innerHTML = json[i]["q1"]
      for (alt in json[i]["options"]) {
        var newRadio = document.createElement('input');
        newRadio.id = json[i]["options"][alt].option_id
        newRadio.value = json[i]["options"][alt].map_valule;
        str += "<input type=\"radio\" name=\"r2\"onclick=\"getRadioValue(value,id)\"value=\"" + newRadio.value + "\" id=\"" + json[i]["options"][alt].option1 + "\" >" + json[i]["options"][alt].option1 + "</input>";
        z.innerHTML = str;
        document.body.appendChild(document.createElement('br'));
      }
    }
  }
}

function generateNextQuestion(value) {
  var main = document.getElementById("mainId");
  var x = document.getElementById("radioDiv");
  var str = ''
  var z = document.getElementById("radioDiv");
  for (var j = 0; j < json.length; j++) {
    if (value == json[j]["map_value"]) {
      document.getElementById("l1").innerHTML = json[j]["q1"]
      for (alt in json[j]["options"]) {
        var newRadio = document.createElement('input');
        newRadio.id = json[j]["options"][alt].option_id
        newRadio.value = json[j]["options"][alt].map_valule;
        str += "<input type=\"radio\" name=\"r2\"onclick=\"getRadioValue(value,id)\"value=\"" + newRadio.value + "\" id=\"" + json[j]["options"][alt].option1 + "\" >" + json[j]["options"][alt].option1 + "</input>";
        z.innerHTML = str;
        document.body.appendChild(document.createElement('br'));
      }
    }
  }
}

function getResponse(value, que) {
  var finalJson = {}
  finalJson["answer"] = que
  var finalJsonArr = []
  var fJsonArr = []
  for (var k = 0; k < json.length; k++) {
    if (value == json[k]["map_value"]) {}
    finalJson["question"] = json[k]["q1"]

  }
  finalJsonArr.push(finalJson)
  var z = fJsonArr.concat(finalJsonArr)
  return fJsonArr;
}

function getRadioValue(value, id) {
  if (value == 0) {
    document.body.innerHTML = "Thank You"
  } else {
    getResponse(value, id)
  }
  generateNextQuestion(value)
}
