const func = require('./utils/function')

let regex = [{
    'name': 'alphabet',
    'allow': 'true',
    'regex': 'a-zA-Z'
  },
  {
    'name': 'number',
    'allow': 'true',
    'regex': '0-9'
  },
  {
    'name': 'whiteSpace',
    'allow': 'true',
    'regex': '\\s'
  },
  {
    'name': 'specialCharacters',
    'allow': 'true',
    'regex': '\\.\\@\\%]'
  },
  {
    'name': 'length',
    'allow': 'true',
    'regex': '{5,9}'
  },
]
loadJson();

function loadJson() {
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'loadJson()', func.logCons.LOG_ENTER)
  try {
    var length = regex.length;
    var regexArray = [];
    var isLength = 0;
    for (let i = 0; i < regex.length; i++) {
      if (regex[i].allow == 'true') {
        if (regex[i].name === 'length' && regex[i].allow === 'true') {
          isLength = 1;
        }
        var array = regex[i].regex;
        regexArray.push(array);
      }
    }
    var expression = join(regexArray, isLength);
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, `Loading Json: ${expression}`)
    return expression;
  } catch (err) {
    func.printLog(func.logCons.LOG_LEVEL_ERROR, `Err while loading json :${err}`)
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'Regular Expression :' + expression, func.logCons.LOG_EXIT)
  }
}

function join(regexArray, isLength) {
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'join()', func.logCons.LOG_ENTER)
  try {
    var element = regexArray.join('');
    var RegExpression;
    if (isLength == 1) {
      RegExpression = '[' + element + '$';
      var regularExpression = new RegExp(RegExpression)
    } else {
      RegExpression = '[' + element + ']*$';
      var regularExpression = new RegExp(RegExpression)
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, `Joining Array: ${regularExpression}`)
    return regularExpression;
  } catch (err) {
    func.printLog(func.logCons.LOG_LEVEL_ERROR, `Err while joining err :${err}`)
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, join(), func.logCons.LOG_EXIT)
  }
}
checkIfValid();

function checkIfValid() {
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'checkIfValid()', func.logCons.LOG_ENTER)
  var exp = loadJson();
  var value = 'qwert1234t.'
  if (exp.test(value)) {
    func.printLog(func.logCons.LOG_LEVEL_INFO, 'valid')
  } else {
    func.printLog(func.logCons.LOG_LEVEL_INFO, 'notvalid')
  }
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'checkIfValid()', func.logCons.LOG_EXIT)
}
