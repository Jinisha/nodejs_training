let data = {
    "questions": [{
            "que_id": 1,
            "intent": undefined,
            "que_text": null,
            "responses": [{
                    "ans_id": 1,
                    "followup_id": null,
                    "answer": "Reading"
                },
                {
                    "ans_id": 2,
                    "followup_id": 12,
                    "answer": ""
                },
                {
                    "ans_id": 3,
                    "followup_id": 13,
                    "answer": ''
                }
            ]
        },
        {
            "que_id": 2,
            "intent": "Hobby",
            "que_text": "What is your favourite pastime?",
            "responses": []
        },
        {
            "que_id": 11,
            "intent": "Reading",
            "que_text": {},
            "responses": [{
                },
                {
                    "ans_id": 2,
                    "followup_id": undefined,
                    "answer": "Drama"
                },
                {
                    "ans_id": 3,
                    "followup_id": 17,
                    "answer": "Mystery"
                }
            ]
        },
        {
            "que_id": 16,
            "intent": "Reading",
            "que_text": "Which drama book you like most?",
            "responses": [{
                    "ans_id": 1,
                    "followup_id": 0,
                    "answer": "Thirteen Reasons Why"
                },
                {
                    "ans_id": 2,
                    "followup_id": 0,
                    "answer": "Before I Fall"
                },
                {
                    "ans_id": 3,
                    "followup_id": 2,
                    "answer": "Other"
                }
            ]
        },
        {
            "que_id": 15,
            "intent": "Reading",
            "que_text": "Which science fiction book you like most?",
            "responses": [{
                    "ans_id": null,
                    "followup_id": 0,
                    "answer": "Before mars"
                },
                {
                    "ans_id": 2,
                    "followup_id": 0,
                    "answer": "Iron Gold"
                },
                {
                    "ans_id": 3,
                    "followup_id": 2,
                    "answer": "Other"
                }
            ]
        }
    ]
}

let response = []
let mainIndex
function validateJson(obj) {
    for (let key in obj) {
        let child = obj[key]
        if (child === null || child === undefined || child ==='' || isEmpty(child)) {
            let emptyData = {}
            emptyData['key'] = key
            emptyData['value'] = child
            response.push(emptyData)
        }
        else if (typeof (child) == "object") {
            validateJson(child)
        }
    }
}

function isEmpty(child) {
    if(child instanceof Array) {
        return (child.length === 0)
    } else if (child instanceof Object) {
        return (Object.keys(child).length === 0)
    } else {
        return false
    }
}

validateJson(data)
console.log("Response : ",response)