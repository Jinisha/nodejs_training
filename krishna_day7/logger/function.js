const winston = require('winston')
const logger = new winston.Logger({
  transports: [
    new(winston.transports.Console)({
      level:'debug',
      timestamp: () => {
        return (new Date()).toLocaleTimeString()
      },
      colorize: true
    })
  ]

})

  
let self = module.exports = {
  logCons: require('./log-constants'),
    /**
   * print log according to profile
   * @param {Integer} level
   * @param {String} msg
   * @param {String} type
   */
  printLog: (level, msg, type) => {
    // TODO:if msg is json then use JSON.stringify()
    // console.log('level',level)
    // console.log('msg',msg)
    // console.log('type',type)    
    if (typeof type !== 'undefined') {
      switch (type) {
        case 0: // ENTER
          msg = self.logCons.LOG_ENTER_INTO_FUNC + msg
          break
        case 1: // EXIT
          msg = self.logCons.LOG_EXIT_FROM_FUNC + msg
          break
      }
    }
    logger.log(level, msg)
    // console.log('level..',level)
    // console.log('msg..',msg)
    // winston.info(msg);
    // console.log(msg);
  }
}  