const winston = require('winston')
var logger =  winston.createLogger({
  transports: [
      new(winston.transports.Console)({
        level:'debug',
        timestamp: () => {
          return (new Date())
        },
        colorize: true
      })
    ]
})
let self = module.exports =
{
  logCons: require('../log-constants'),
  printLog: (level, msg, type) => {
      logger.log(level, msg)
    }
}
