let a=10;
let b=7;

var promise_check = new Promise(function(resolve,reject){
    if(b === 0)
    {
        reject("not possible");
    }
    else
    {
        resolve(a/b);
    }

});

promise_check.then(function(div){
    console.log(div);
}).catch(function(str){
    console.log(str);
});
