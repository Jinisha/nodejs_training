function displayEmailMaskTest() {
  var maskedid = "";
  var replaceEmail = document.getElementById("replaceEmailTask").value
  var myemailId = document.getElementById("emailMask").value
  var prefix = myemailId.substring(0, myemailId.lastIndexOf("@"));
  var postfix = myemailId.substring(myemailId.lastIndexOf("@"));

  for (var i = 0; i < prefix.length; i++) {
    if (i == 0 || i == prefix.length - 1) {
      maskedid = maskedid + prefix[i].toString();
    } else {
      maskedid = maskedid + replaceEmail;
    }
  }
  maskedid = maskedid + postfix;
  document.getElementById("emailMaskValue").innerHTML = "Masked Email= " + maskedid
  console.log(maskedid);
}

function displayPhoneMaskTest() {
  var phoneReplace = document.getElementById("replacePhoneTask").value
  var mainStr = document.getElementById("phoneMask").value
  vis = mainStr.slice(-4),
    countNum = '';

  for (var i = (mainStr.length) - 4; i > 0; i--) {
    countNum += phoneReplace;
  }

  document.getElementById("phoneMaskValue").innerHTML = "Phone mask value= " + countNum + vis
}

function displayNameMaskTest() {
  var firLet
  var replaceChar
  var array = []
  var combineLetter;
  var nameStr = document.getElementById("nameMask").value
  var replaceNameChar = document.getElementById("replaceNameTask").value
  var strLen = nameStr.length
  var splitStr = nameStr.split(" ")
  for (var i = 0; i < splitStr.length; i++) {
    var value = splitStr[i]
    for (var j = 1; j <= value.length; j++) {
      firLet = value.charAt(0)
      var newString = value.substr(1);
      replaceChar = Array(newString.length + 1).join(replaceNameChar);
      combineLetter = firLet + replaceChar
    }
    array.push(combineLetter)
  }
  document.getElementById("nameMaskValue").innerHTML = "Name Mask Value= " + array.join(' ')
}
