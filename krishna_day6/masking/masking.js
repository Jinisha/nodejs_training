function maskEmail(){
    var maskingChar = document.getElementById('character').value    
    var maskedId = "";
    var email = document.getElementById('email').value    
    var index = email.lastIndexOf("@");
    var preString = email.substring(0, index);
    var postString = email.substring(index);
    var mask = preString.split('').map(function(o, i) {
      if (i == 0 || i == (index - 1)) {          
        return o;
      } else {
        return maskingChar;
      }
    }).join('');
    maskedId = mask + postString;
    document.getElementById('emailOutput').innerHTML = maskedId
}
function maskContact(){
    var maskingChar = document.getElementById('character').value    
    var contact = document.getElementById('contact').value
    digits = contact.slice(-4),
    countNum = '';
    if(contact.length === 10){
        for(var i = (contact.length)-4; i>0; i--){
            countNum += maskingChar;
        }
        document.getElementById('contactOutput').innerHTML = countNum+digits
    }
    else{
        document.getElementById('contactOutput').innerHTML = 'Enter correct Number'
    }
}
function maskText(){
    var maskingChar = document.getElementById('character').value    
    var text = document.getElementById('text').value
    var firstName = text.substr(0,text.indexOf(' '))
    var lastName = text.substr(text.indexOf(' ')+1)
    var maskedFirstName= firstName.split('').map(function(char,i){
        if(i==0){
            return firstName[i]
        }else{
            return maskingChar
        }
    }).join('')    
    var maskedLastName= lastName.split('').map(function(char,i){
        if(i==0){
            return lastName[i]
        }else{
            return maskingChar
        }
    }).join('')
    var maskedName = maskedFirstName +' ' + maskedLastName
    document.getElementById('textOutput').innerHTML = maskedName
}
