var json = {
    "regularexpcondition": [{
            "id": 1,
            "regName": "Alphabets",
            "active": "true",
            "condition": "a-zA-Z0-9"
        },
        {
            "id": 2,
            "regName": "numeric",
            "active": "false",
            "condition": "0-9"
        },
        {
            "id": 3,
            "regName": "specialcharactor",
            "active": "true",
            "condition": "\\@\\."
        },
        {
            "id": 5,
            "regName": "whitespace",
            "active": "true",
            "condition": "\\s"
        },
        {
            "id": 4,
            "regName": "length",
            "active": "true",
            "condition": "{5,15}"
        }
    ]
}

function getRegularExp() {
    var value = document.getElementById("text1").value;
    var allPattern = "";
    var getarrayreg;
    getarrayreg = json.regularexpcondition;
    for (var i = 0; i < getarrayreg.length; i++) {
        if (getarrayreg[i].active === 'true') {
            allPattern += getarrayreg[i].condition;
            if (i === 3) {
                allPattern = "[" + allPattern + "]"
            }
        }
    }
    var addexp = `^${allPattern}$`;
    document.getElementById('pattern').innerHTML = "Pattern is = " + addexp + "<br>";
    var patt = new RegExp(addexp);
    var result = patt.test(value);
    document.getElementById('resultid').innerHTML = "Regular Expression is = " + result + "<br>";
}