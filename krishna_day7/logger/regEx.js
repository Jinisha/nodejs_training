const func = require('./function')
let json = [
    {
      "field":"alphabets",
      "value":true,
      "pattern":"a-zA-Z"
    },
    {
      "field":"numbers",
      "value":true,
      "pattern":"0-9"
    },
    {
        "field":"whiteSpace",
        "value":true,
        "pattern":"\\s"
    },
    {
        "field":"characters",
        "value":true,
        "pattern":"\\.\\$\\@"
    },
    {
        "field":"length",
        "value":true,
        "pattern":"{5,20}"
    }
]
let regExpression;
function generateRegExpression(){
    func.printLog(func.logCons.LOG_LEVEL_DEBUG,'generateRegExpression()', func.logCons.LOG_ENTER)
    let regExpstring = '^['
    let flag = false
    let length
    json.filter(function(element){
        if(element.value === true && element.field!=='length'){
            regExpstring += element.pattern
        }
        if(element.field === 'length'){
            flag = true
            length = element.pattern
        }
    });    
    if(flag === true){
            regExpstring += ']'+length+'$'
    }else{
        regExpstring += ']*$' 
    }
    regExpression = new RegExp(regExpstring)        
    func.printLog(func.logCons.LOG_LEVEL_INFO,regExpression, func.logCons.LOG_ENTER)                 
     validate()
     func.printLog(func.logCons.LOG_LEVEL_DEBUG,'generateRegExpression()', func.logCons.LOG_EXIT)
     
}  
function validate(){
    func.printLog(func.logCons.LOG_LEVEL_INFO,'validate()', func.logCons.LOG_ENTER)    
    let stringToValidate = ''
    if(stringToValidate){
    if(regExpression.test(stringToValidate))
        func.printLog(func.logCons.LOG_LEVEL_INFO,'String is matched with RegEx', func.logCons.LOG_ENTER)            
    else
        func.printLog(func.logCons.LOG_LEVEL_INFO,'String is not matched with RegEx', func.logCons.LOG_ENTER)            
    }
    else
        func.printLog(func.logCons.LOG_LEVEL_ERROR,'String is undefined', func.logCons.LOG_ENTER)            
    func.printLog(func.logCons.LOG_LEVEL_INFO,'validate()', func.logCons.LOG_EXIT)        
}
generateRegExpression()