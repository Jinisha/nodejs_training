const func = require('./utils/function')

let regex = [{
    'name': 'alphabet',
    'condition': 'true',
    'regex': 'a-zA-Z'
  },
  {
    'name': 'number',
    'condition': 'true',
    'regex': '0-9'
  },
  {
    'name': 'whiteSpace',
    'condition': 'true',
    'regex': '\\s'
  },
  {
    'name': 'specialCharacters',
    'condition': 'true',
    'regex': '\\.\\@\\%]'
  },
  {
    'name': 'length',
    'condition': 'true',
    'regex': '{5,9}'
  },
]
load();

function load() {
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'load()', func.logCons.LOG_ENTER)
  try {
    var length = regex.length;
    var regexArray = [];
    var isLength = 0;
    for (let i = 0; i < regex.length; i++) {
      if (regex[i].condition == 'true') {
        if (regex[i].name === 'length' && regex[i].condition === 'true') {
          isLength = 1;
        }
        var array = regex[i].regex;
        regexArray.push(array);
      }
    }
    var expression = join(regexArray, isLength);
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, `Loading Json: ${expression}`)
    return expression;
  } catch (err) {
    func.printLog(func.logCons.LOG_LEVEL_ERROR, `Err while loading json :${err}`)
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'Regular Expression :' + expression, func.logCons.LOG_EXIT)
  }
}

function join(regexArray, isLength) {
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'join()', func.logCons.LOG_ENTER)
  try {
    var element = regexArray.join('');
    var RegExpression;
    if (isLength == 1) {
      RegExpression = '[' + element + '$';
      var regularExpression = new RegExp(RegExpression)
    } else {
      RegExpression = '[' + element + ']*$';
      var regularExpression = new RegExp(RegExpression)
      show.innerHTML = "Regular Expression  is" + " " + regularExpression;
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, `Joining Array: ${regularExpression}`)
    return regularExpression;
  } catch (err) {
    func.printLog(func.logCons.LOG_LEVEL_ERROR, `Err while joining err :${err}`)
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, join(), func.logCons.LOG_EXIT)
  }
}
checkValid();

function checkValid() {
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'checkValid()', func.logCons.LOG_ENTER)
  var exp = load();
  var value = 'qwert1234t.'
  if (exp.test(value)) {
    func.printLog(func.logCons.LOG_LEVEL_INFO, 'valid')
  } else {
    func.printLog(func.logCons.LOG_LEVEL_INFO, 'notvalid')
  }
  func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'checkValid()', func.logCons.LOG_EXIT)
}
