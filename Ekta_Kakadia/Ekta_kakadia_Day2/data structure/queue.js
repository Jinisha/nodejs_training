'use strict'

function Queue(data) {
  this.next = null;
  this.data = parseInt(data, 10);
}

function OrderedList() {
  this.oldest_index = 1;
  this.new_index = 1;
  this.storage = {};
}
var list = new OrderedList();
OrderedList.prototype.size = function() {
  var output3 = document.getElementById("size");
  var s1 = this.new_index - this.oldest_index
  output3.innerHTML = "Size of queue :" + s1;
};
OrderedList.prototype.enqueue = function(id_add) {
  var output1 = document.getElementById("add");
  this.storage[this.new_index] = id_add;
  //output1.innerHTML="value in queue :" + this.storage[this.new_index];
  if (this.new_index) {
    if (this.new_index == 1) {
      output1.innerHTML = this.storage[this.new_index];
    } else {
      output1.innerHTML += "," + this.storage[this.new_index];
    }
  }
  this.new_index++;
};
OrderedList.prototype.dequeue = function() {
  var output = document.getElementById("delete");
  var oldest_index = this.oldest_index,
    new_index = this.new_index,
    deletedData;
  if (oldest_index != new_index) {
    deletedData = this.storage[oldest_index];
    delete this.storage[oldest_index];
    for (var i = this.oldest_index + 1; i < this.new_index; i++) {
      if (i <= 2) {
        output.innerHTML += this.storage[i] + ",";
      } else {
        output.innerHTML += this.storage[i];
      }
    }
    this.oldest_index++;
  }
};
