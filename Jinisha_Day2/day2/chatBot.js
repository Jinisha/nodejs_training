let javaobj;                                                          //javaobj stores json obj


//Laod JSON file into javaobj
// call to renderQuestion function
 function loadJson()
        {
          var xhttp = new XMLHttpRequest();
          xhttp.onreadystatechange = function(){
          if(xhttp.readyState==4 && xhttp.status==200)
              {
                   javaobj = JSON.parse(xhttp.response);
                   renderQuestion(1);
              }
          };
          xhttp.open("GET","questions.json",true);
          xhttp.send();
        }

  //render question on webpage
  function renderQuestion(id)
        {
          if(id!=0){
          let str = "";
          var id=id-1;
            var queArea = document.getElementById("question_div");
            queArea.innerHTML=javaobj.questions[id].que;
            var optArea = document.getElementById("options_div");
            for(var i=0;i<javaobj.questions[id].response.length;i++)
            {
              var r_id = javaobj.questions[id].response[i].followup_id;
              var display = javaobj.questions[id].response[i].ans;
              
                str +="<input type=\"radio\" name=\"r_button\" id=\""+ r_id +"\" >"+ display +"</input>";
            }
            optArea.innerHTML = str;
          }
          else{
            document.getElementById("question_div").innerHTML="thank you";
            document.getElementById("options_div").innerHTML="completed";
            document.getElementById('nex').style.visibility = 'hidden';
          }
        }

  //render next question
  function renderNext()
        {
          var radioButton = document.getElementsByName("r_button");
          for(var i=0;i<radioButton.length;i++)
          {
            if(radioButton[i].checked){
              var followup_id = radioButton[i].id;
            }
          }
          renderQuestion(followup_id);
        }

  
