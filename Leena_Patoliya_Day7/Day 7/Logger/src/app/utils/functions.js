const winston = require('winston')
const logger = new winston.Logger({
  transports: [
    new(winston.transports.Console)({
      level:'debug',
      timestamp: () => {
        return (new Date())
      },
      colorize: true
    })
  ]

})

module.exports = {
  logCons: require('./constants/log-constants'),
  printLog: (level, msg, type) => {
    logger.log(level, msg)
  }
}
