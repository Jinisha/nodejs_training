var func = require('../utils/functions');
mydata = [
    {
        "condition": "alphabet",
        "status": "true"
    },
    {
        "condition": "numeric",
        "status": "true"
    },
    {
        "condition": [
            {
                "sp": "$"
            },
            {
                "sp": "."
            }
        ],
        "status": "true"
    },
    {
        "condition": "white_space",
        "status": "true"
    },
    {
        "len": "{3,10}",
        "status": "true"
    }
];



var input;
var regular_expression = "";
var i = 0;
var condition_true = ['false', 'false', 'false', 'false', 'false'];
// create JSON object

myFunction();

function myFunction() {
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'myFunction()',func.logCons.LOG_ENTER)
    for (i = 0; i < mydata.length; i++) {
        condition_true[i] = mydata[i].status;
    }
    regular_expression = form_regular_expression(condition_true);
   // document.getElementById('show_expression').innerHTML = regular_expression;
   func.printLog(func.logCons.LOG_LEVEL_INFO,'regular expression = ' + regular_expression)  
    check();
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'myFunction()',func.logCons.LOG_EXIT)
}

function form_regular_expression(condition_true) {
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'form_regular_expression()',func.logCons.LOG_ENTER)
    var final_expression = "^[";
    for (i = 0; i < (mydata.length - 1); i++) {
        if (condition_true[i] === "true") {
            switch (i + 1) {
                case 1: final_expression += "\\w";
                    break;
                case 2: final_expression += "\\d";
                    break;
                case 3:
                    for (i = 0; i < 2; i++) {
                        final_expression += "\\";
                        final_expression += mydata[2].condition[i].sp;
                    }
                    break;
                case 4:
                    final_expression += "\\s";
                    break;
            }
        }
    }
    final_expression += "]";

    if (condition_true[4] === "true") {
        final_expression += (mydata[4].len + "$");
    }
    else {
        final_expression += "*$";
    }
    return final_expression;
    
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'form_regular_expression()',func.logCons.LOG_EXIT)
}

function check() 
{
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'check()',func.logCons.LOG_ENTER)
   // input = document.getElementById('TextBox1').value;
    regular_expression = new RegExp(regular_expression);
    
    //document.getElementById('show_result').innerHTML = 
    if(regular_expression)
    {
    if(regular_expression.test(input)){
        func.printLog(func.logCons.LOG_LEVEL_INFO,'String is matched with RegEx')         
    }
    else{
        func.printLog(func.logCons.LOG_LEVEL_INFO,'String is not matched with RegEx')            

    }}
    else{
        func.printLog(func.logCons.LOG_LEVEL_ERROR,'String is undefined')            
    }
    func.printLog(func.logCons.LOG_LEVEL_DEBUG, 'check()',func.logCons.LOG_EXIT)
}

