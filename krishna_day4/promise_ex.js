function getnumber(a,b){
    var result= a/b;
    var promise = new Promise(function(accept,decline){
    if(result != Infinity){
        accept(result);
    }
    else{
        decline(Error(result));
    }
});
promise.then(function(accept) {
    console.log(accept);  
  }, function(decline) {
    console.log(decline);  
  });
}
getnumber(100,4)