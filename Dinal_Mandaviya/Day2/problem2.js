var finalArr = new Array(4);
var myGlobalArray = new Array(4);

function generateArray() {
  var arr = [];
  var j;
  while (arr.length < 16) { // generate random values
    var count = 0
    a = Math.floor(Math.random() * 16)
    a = Number.parseInt(a)
    for (j = 0; j < arr.length; j++) {
      if (a == arr[j]) {
        count++
      }
    }
    if (count == 0) {
      arr.push(a)
    }
  }
  for (var i = 0; i < 4; i++) {
    finalArr[i] = new Array(4)
  }
  for (var i = 0, k = 0; i < 4; i++) { // print matrix
    for (var j = 0; j < 4; j++, k++) {
      finalArr[i][j] = arr[k];
      document.getElementById('p1').innerHTML += finalArr[i][j] + '  '
    }
    document.getElementById('p1').innerHTML += ' <br>'
  }
}

function suffleArray() {
  var arr = [];
  var j;
  while (arr.length < 16) { // generate random values
    var count = 0
    a = Math.floor(Math.random() * 16)
    a = Number.parseInt(a)
    for (j = 0; j < arr.length; j++) {
      if (a == arr[j]) {
        count++;
      }
    }
    if (count == 0) {
      arr.push(a);
    }
  }
  for (var i = 0; i < 4; i++) {
    myGlobalArray[i] = new Array(4)
  }
  for (var i = 0, k = 0; i < 4; i++) { // print matrix
    for (var j = 0; j < 4; j++, k++) {
      myGlobalArray[i][j] = arr[k]
      document.getElementById('p2').innerHTML += myGlobalArray[i][j] + '  '
    }
    document.getElementById('p2').innerHTML += ' <br>'
  }
}

function selectrows() {
  var selrow1 = []
  var selrow2 = []
  var r1 = document.getElementById('row1').value
  var r2 = document.getElementById('row2').value
  for (var i = 0; i < finalArr.length; i++) {
    selrow1.push(finalArr[r1][i])
  }
  for (var j = 0; j < myGlobalArray.length; j++) {
    selrow2.push(myGlobalArray[r2][j])
  }
  output(selrow1, selrow2)
}

function output(selrow1, selrow2) {
  var cardnum = [];
  for (var k = 0; k < 4; k++) {
    if (selrow1[k] == selrow2[k]) {
      var m1 = document.getElementById('msg')
      m1.innerHTML = 'Number is: ' + selrow1[k]
      cardnum.push(selrow1[k]);
    }
  }
  if (cardnum.length != 0) {
    var m1 = document.getElementById('msg')
    m1.innerHTML = 'Bad magician!!'
  } else {
    var m1 = document.getElementById('msg')
    m1.innerHTML = 'Volunteer cheated!!'
  }
}