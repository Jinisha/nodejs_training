var finalArr = new Array(4)
var selectrow = new Array(4)

function generateArray () {
  var arr = [];
  var j,num;
  while (arr.length < 16) {
    var count = 0
    num = Math.floor(Math.random() * 2)
    num = Number.parseInt(num)
    arr.push(num)  }
  for (var i = 0; i < 4; i++) {
    finalArr[i] = new Array(4)
  }
  for (var i = 0, k = 0; i < 4; i++) {
    for (var j = 0; j < 4; j++, k++) {
      finalArr[i][j] = arr[k]
      if (finalArr[i][0] == 1) {
        selectrow.push(finalArr[i][j])
      } else {
        selectrow.splice(finalArr[i][j], 0)
      }
      document.getElementById('matrix').innerHTML += finalArr[i][j]
    }
    document.getElementById('matrix').innerHTML += ' <br>'
  }
  checkMaxBinaryNumber(selectrow);
}

function checkMaxBinaryNumber (selectrow) {
  var convertDecimal = [];
  for (var i = 0;i < 4;i++) {
    convertDecimal.push(parseInt(selectrow[i], 2).toString(10));
  }
  var maxnum = Math.max(...convertDecimal);
  var index = selectrow.indexOf(maxnum);
  document.getElementById('rowid').innerHTML = index;
}
