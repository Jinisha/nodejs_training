function display() {
  var ranNumber
  var matrixArray = []
  for (var i = 0; i < 9; i++) {
    ranNumber = Math.floor(Math.random() * (1 - 0 + 1)) + 0;
    matrixArray.push(ranNumber)
    var row1 = []
    var row2 = []
    var row3 = []
    var row4 = []
    var valueArray = []
    var finalMatrix = []
    var index;
    for (var j = 0; j < 3; j++) {
      row1.push(matrixArray[j])
    }
    var joinValue1 = Number(row1.join(''))
    var v1 = parseInt(joinValue1, 2).toString(10);
    finalMatrix.push(row1)
    valueArray.push(v1)
    for (var k = 3; k < 6; k++) {
      row2.push(matrixArray[k])
    }
    var joinValue2 = Number(row2.join(''))
    var v2 = parseInt(joinValue2, 2).toString(10);
    finalMatrix.push(row2)
    valueArray.push(v2)
    for (var l = 6; l < 9; l++) {
      row3.push(matrixArray[l])
    }
    var joinValue3 = Number(row3.join(''))
    var v3 = parseInt(joinValue3, 2).toString(10);
    finalMatrix.push(row3)
    valueArray.push(v3)
    findIndex(valueArray)
    for (var z = 0; z < finalMatrix.length; z++) {
      document.getElementById("p" + z).innerHTML = finalMatrix[z]
    }
  }
}

function findIndex(value) {
  var max = Math.max(...value);
  for (var j = 0; j < value.length; j++) {
    if (value[j] == max) {
      document.getElementById("index").innerHTML = "Index Number:" + j
    }
  }
  document.getElementById("result").innerHTML = "Maximum Value:" + max

}
