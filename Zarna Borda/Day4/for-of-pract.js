let json  = [{
        "fname": "zarna",
        "lname": "borda",
        "marks": 90,
    },
    {
        "fname": "jinisha",
        "lname": "kheni",
        "marks": 88,
    },
    {
        "fname": "jd",
        "lname": "galani",
        "marks": 100,
    },
    {
        "fname": "paras",
        "lname": "korat",
        "marks": 60,
    },
    {
        "fname": "zarna",
        "lname": "borda",
        "marks": 50,
    }
];

function operate() {
    let max = 0, index;
    for (let value of json) {
        if(value.marks > max) {
            max = value.marks;
            index = json.indexOf(value);
        }
    }
    console.log(
        `fname: ${json[index].fname} lname: ${json[index].lname} marks: ${json[index].marks}`
    );
}
operate();