function intro() {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('patel');
      }, 2000);
    });
  }
  
  async function asyncCall() {
    console.log('dhwani');
    var result = await intro();
    console.log(result);
    // expected output: "resolved"
  }
  
  asyncCall();
  