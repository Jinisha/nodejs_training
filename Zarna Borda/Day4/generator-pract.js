/**
 * 
 * Fibbonacci series using generator 
 */
function* generatorForLoop(num) {
  let a = 0,b = 1;
  console.log(a);
  console.log(b);
  while(true) {
    let c = a + b;
    a = b;
    b = c;
    yield console.log(c);
  }
}

const genForLoop = generatorForLoop(10);

function fibbonacci() {
  for (let i = 0; i < 10; i += 1) {
    genForLoop.next(); // first console.log - 0
  }
}
fibbonacci();

