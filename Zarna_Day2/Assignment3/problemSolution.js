/**
 * Load external json file data
 * Immediately invoked on load file problemSolution.js
 */
let json = function () {
    let jsonTemp = null;
    $.ajax({
        'async': false,
        'url': "question.json",
        'dataType': "json",
        'success': function (data) {
            jsonTemp = data;
        }
    });
    return jsonTemp;
}();

let responseArray;

/**
 * Start chatbot
 */
function start () {
    responseArray = new Array();
    if (document.getElementById("queDiv")) {
        removeElement();
    }
    //display 1st question
    displayQuestion(1);
}

/**
 * Display question on previous response
 * @param {int} followupId 
 */
function displayQuestion (followupId) {
    /**
     * if no any related question of previous answer
     * then exit
     * else display related question
     */
    if (followupId == 0) {
        var div = document.createElement("div");
        div.setAttribute("id", "queDiv");
        var h1 = document.createElement("h1");
        h1.appendChild(document.createTextNode("Thank you..!!"));
        div.appendChild(h1).appendChild(document.createElement("br"));
        document.body.appendChild(div);
        displayResponse();
    } else {
        createQuestion();
        renderNext(followupId);
        createSubmitButton();
    }
}

/**
 * Submit response and display new question
 */
function submitResponse () {
    let ischecked = false;
    var answer = document.getElementsByName("answer");
    var label = document.getElementsByTagName("label");
    var followupId, response;
    for (var i = 0; i < answer.length; i++) {
        if (answer[i].checked) {
            ischecked = true;
            followupId = answer[i].value;
            response = label[i].textContent;
        }
    }
    createResponse(ischecked, followupId, response);
}

/**
 * Create response of user 
 * @param {boolean} ischecked 
 * @param {int} followupId 
 * @param {string} response 
 */
function createResponse (ischecked, followupId, response) {
    if (ischecked) {
        var obj = {
            "que_text": document.getElementById("question").innerHTML,
            "followup_id": followupId,
            "answer": response,
        }
        responseArray.push(obj);
        removeElement();
        displayQuestion(followupId);
    } else {
        removeElement();
        var div = document.createElement("div");
        div.setAttribute("id", "queDiv");
        var h1 = document.createElement("h1");
        h1.appendChild(document.createTextNode("Thank you..!!"));
        div.appendChild(h1);
        div.appendChild(document.createElement("br"));
        document.body.appendChild(div);
        displayResponse();
    }
}

/**Render next related question  */
function renderNext (followupId) {
    for (var i = 0; i < Object.keys(json.questions).length; i++) {
        if (json.questions[i].que_id == followupId) {
            document.getElementById("question").innerHTML = json.questions[i].que_text;
            var radioId = 1;
            console.log(json.questions[i].responses.length);
            for (var j = 0; j < json.questions[i].responses.length; j++) {
                var answer = json.questions[i].responses[j].answer;
                var value = json.questions[i].responses[j].followup_id;
                createRadioButton(radioId, answer, value);
                radioId += 1;
            }
        }
    }
}

/**
 * Display user response
 */
function displayResponse () {
    var label = document.createElement("label");
    label.appendChild(document.createTextNode("Your responses: "));
    document.getElementById("queDiv").appendChild(label);
    document.getElementById("queDiv").appendChild(document.createElement("br"));
    for (let response of responseArray) {
        var label2 = document.createElement("label");
        label2.appendChild(document.createTextNode(`Question : ${response.que_text} Answer: ${response.answer}`));
        document.getElementById("queDiv").appendChild(label2).appendChild(document.createElement("br"));
    }
}

/**
 * Create question element
 */
function createQuestion () {
    var div = document.createElement("div");
    div.setAttribute("id", "queDiv");
    var h1 = document.createElement("h1");
    h1.setAttribute("id", "question");
    div.appendChild(h1);
    var divRadio = document.createElement("div");
    divRadio.setAttribute("id", "radioDiv");
    div.appendChild(divRadio);
    document.body.appendChild(div);
}

/**
 * Create Radio Button
 * @param {id} id 
 * @param {Questiontext} text 
 * @param {id} followupId 
 */
function createRadioButton (id, text, followupId) {
    var radio = document.createElement("input");
    radio.setAttribute("type", "radio");
    radio.setAttribute("id", "response" + id);
    radio.setAttribute("name", "answer");
    radio.setAttribute("value", followupId);
    radio.setAttribute("checked", false);
    var label = document.createElement("label");
    label.setAttribute("id", "answer" + id);
    label.setAttribute("for", "response" + id);
    label.appendChild(radio);
    label.appendChild(document.createTextNode(text));
    document.getElementById("radioDiv").appendChild(label).appendChild(document.createElement("br"));
    clearRadioSelection();
}

/**
 * Create submit Button
 */
function createSubmitButton () {
    var button = document.createElement("button");
    button.setAttribute("id", "button");
    button.appendChild(document.createTextNode("Next"));
    document.getElementById("queDiv").appendChild(button);
    document.getElementById("button").onclick = submitResponse;
}

/**
 *Clear previous radio selection 
 */
function clearRadioSelection () {
    var radio = document.getElementsByName("answer");
    for (var i = 0; i < radio.length; i++)
        radio[i].checked = false;
}

/**
 * Remove created elements 
 */
function removeElement () {
    var element = document.getElementById("queDiv");
    element.parentNode.removeChild(element);
}