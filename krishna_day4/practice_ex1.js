function fun1(a,b,callback){
    result = (a*b) + (a+b);
    callback(null,result);
}
function fun2(){
    fun1(5,4,function(err,result){
        console.log(result);
    });
}
fun2();