let regex = [{
    "name": "alphabet",
    "allow": "true",
    "regex": "a-zA-Z"
  },
  {
    "name": "number",
    "allow": "true",
    "regex": "0-9"
  },
  {
    "name": "whiteSpace",
    "allow": "true",
    "regex": "\\s"
  },
  {
    "name": "specialCharacters",
    "allow": "true",
    "regex": "\\.\\@\\%]"
  },
  {
    "name": "length",
    "allow": "true",
    "regex": "{5,9}"
  },
]

function loadJson() {
  var length = regex.length;
  var regexArray = [];
  var isLength = 0;
  for (let i = 0; i < regex.length; i++) {
    if (regex[i].allow == "true") {
      if (regex[i].name === 'length' && regex[i].allow === 'true') {
        isLength = 1;
      }
      var array = regex[i].regex;
      regexArray.push(array);
    }
  }
  var expression = join(regexArray, isLength);
  return expression;
}

function join(regexArray, isLength) {
  var element = regexArray.join("");
  var show = document.getElementById("show");
  var RegExpression;
  if (isLength == 1) {
    RegExpression = "[" + element + "$";
    var regularExpression = new RegExp(RegExpression)
    show.innerHTML = "Regular Expression  is" + " " + regularExpression;
  } else {
    RegExpression = "[" + element + "]*$";
    var regularExpression = new RegExp(RegExpression)
    show.innerHTML = "regular Expression is" + " " + regularExpression;
  }
  return regularExpression;
}

function checkIfValid() {
  var exp = loadJson();
  var showValidOrNot = document.getElementById("showValidOrNot");
  var value = document.getElementById("value").value;
  if (exp.test(value)) {
    showValidOrNot.innerHTML = "Valid";
  } else {
    showValidOrNot.innerHTML = "Not Valid";
  }
}
