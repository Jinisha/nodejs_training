module.exports={
    LOG_LEVEL_ERROR:'error',
    LOG_LEVEL_WARN:'warn',
    LOG_LEVEL_INFO:'info',
    LOG_LEVEL_VERBOSE: 'verbose', 
    LOG_LEVEL_DEBUG: 'debug', 
    LOG_LEVEL_SILLY: 'silly', 

    LOG_ENTER_INTO_FUNC: 'ENTER: ',
    LOG_PARAM: 'PARAM: ',
    LOG_EXIT_FROM_FUNC: 'EXIT: ',
    LOG_ENTER: 0,
    LOG_EXIT: 1
}