function square(a, b) {
    return new Promise(resolve => {
        setTimeout(() => {
            resolve(a / b);
        }, 2000);
    });
}
async function asyncCall() {
    var c = await square(200, 100);
    if (c != 0) {
        console.log("number of block is:"+c);
    } else {
        console.log("Error")
    }
}
asyncCall();