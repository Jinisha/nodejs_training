const winston = require('winston')
const logger = new winston.Logger({
  transports: [
    new(winston.transports.Console)({
      level:'debug',
      timestamp: () => {
        return (new Date()).toLocaleTimeString()
      },
      colorize: true
    })
  ]

})

  
let self = module.exports = {
  logCons: require('./log-constants'),

  printLog: (level, msg, type) => {
   
    if (typeof type !== 'undefined') {
      switch (type) {
        case 0: 
          msg = self.logCons.LOG_ENTER_INTO_FUNC + msg
          break
        case 1: 
          msg = self.logCons.LOG_EXIT_FROM_FUNC + msg
          break
      }
    }
    logger.log(level, msg)
   
  }
}  
