function generateMatrix() {
  var aInput = document.getElementById("a").value;
  var bInput = document.getElementById("b").value;
  var uniqueArray;
  var value1 = [];
  var value2 = [];
  value1 = displayMatrix1(aInput);
  value2 = displayMatrix2(bInput);
  matchRows(value1, value2);
}

function generateRandom() {
  var max = 16;
  var uniqueArray = 0;
  var min = 1;
  var randNumArr = [];
  var len = uniqueArray.length;
  for (var r = 0; len != 16; r++) {
    var randnum = Math.floor(Math.random() * (max - min + 1) + min);
    randNumArr.push(randnum);
    uniqueArray = randNumArr.filter(function(elem, pos) {
      return randNumArr.indexOf(elem) == pos;
    });
    var len = uniqueArray.length;
  }
  return uniqueArray;
}

function displayMatrix2(aInput) {
  var uniqueArray = generateRandom();
  var a = [];
  var matrix;
  var c = [];
  var showMatrix = document.createElement('p');
  showMatrix.innerHTML = "Matrix is";
  document.body.appendChild(showMatrix);
  for (var i = 0; i < 16; i++) {
    matrix = uniqueArray[i];
    a.push(matrix);
    if (a.length === 4) {
      c.push(a);
      var show = document.createElement('p');
      show.innerHTML = a + " ";
      document.body.appendChild(show);
      a = [];
    }
  }
  var row1 = getRowNumber1(uniqueArray, aInput, c);
  return row1;
}

function displayMatrix1(bInput) {
  var uniqueArray = generateRandom();
  var matrix;
  var a = [];
  var c = [];
  var showMatrix = document.createElement('p');
  showMatrix.innerHTML = "Matrix is";
  document.body.appendChild(showMatrix);
  for (var i = 0; i < 16; i++) {
    matrix = uniqueArray[i];
    a.push(matrix);
    if (a.length === 4) {
      c.push(a);
      var show = document.createElement('p');
      show.innerHTML = a + " ";
      document.body.appendChild(show);
      a = [];
    }
  }
  var row2 = getRowNumber2(uniqueArray, bInput, c);
  return row2;
}

function getRowNumber1(uniqueArray, aInput, c) {
  var numArray1 = [];
  var a;
  loop1: for (t = c.length; t < c.length + 4; t++) {
    a = uniqueArray[t];
    numArray1.push(a);
    if (numArray1.length == 4) {
      break loop1;
    }
  }
  var showRowElements1 = document.createElement('p');
  showRowElements1.innerHTML = "row1 is" + "" + numArray1 + " ";
  document.body.appendChild(showRowElements1);
  return numArray1;
}

function getRowNumber2(uniqueArray, bInput, c) {
  var numArray2 = [];
  var a;
  loop1: for (t = c.length; t < c.length + 4; t++) {
    a = uniqueArray[t];
    numArray2.push(a);
    if (numArray2.length == 4) {
      break loop1;
    }
  }
  var showRowElements2 = document.createElement('p');
  showRowElements2.innerHTML = "row2 is" + "" + numArray2 + " ";
  document.body.appendChild(showRowElements2);
  return numArray2;
}

function matchRows(row1, row2) {
  var matchedEle;
  var a = [];
  for (var y = 0; y < row2.length; y++) {
    if (row1[y] == row2[y]) {
      // var index = row2.indexOf(row1[x]);
      matchedEle = row1[y];
      a.push(matchedEle);
    }
  }
  if (a.length > 1) {
    var show = document.createElement('p');
    show.innerHTML = "Volunteer cheated";
    document.body.appendChild(show);
  } else if (a.length == 0) {
    var show = document.createElement('p');
    show.innerHTML = "Bad Magician";
    document.body.appendChild(show);
  } else {
    if (a.length == 1) {
      var show = document.createElement('p');
      show.innerHTML = "Element is" + a;
      document.body.appendChild(show);
    }
  }
  return false;
}
